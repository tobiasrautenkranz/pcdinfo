pcdinfo is a command line tool to get metadata from Kodak Photo CD "info.pcd" files.

It is best used to check the number of images reported by the iso9660 file system of the Photo CD.
To convert the images you can use pcdtojpeg https://sourceforge.net/projects/pcdtojpeg/

BUILD

  Building the program requires a C compiler and make. Run:
  
  $ make
  
  to generate the pcdinfo program. You can test for regression errors
  using $ make check


USAGE
 
  To get the metadata mount the Kodak Photo CD and supply the path to the info.pcd file
  in the photo_cd directory of the disk as argument to pcdinfo. E.g.:
  
  $ pcdinfo /mnt/cdrom/photo_cd/info.pcd

   revision                   0.6
   serial number              000609151992
   creation time              Tue Sep 15 20:52:44 1992
   modification time          Tue Sep 15 20:52:44 1992
   number of images           116
   interleave ratio           1
   resolution                 0x20
   lead out start time        73:25:26
   number of sessions         1
  

SOURCE Code

  The most recent source code is at:
   
   https://gitlab.com/tobiasrautenkranz/pcdinfo
    
CONTACT

  For comments, ideas, etc. you can contact me at tobias@rautenkranz.ch
