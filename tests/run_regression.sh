#!/usr/bin/env bash
#
# Regression tests against Kodak Photo CD Sampler
# https://archive.org/download/kodak-photo-cd-player-demo-disc
#
# rights.use
#
#                          PHOTOGRAPHIC QUALITY IMAGES
#
#                     Copyright Eastman Kodak Company, 1991
#
#   Authorized holders of this Photo CD sampler disc may use, store transmit,
#   reproduce and display the recorded images for any purpose associated with
#   electronic imaging.  Unless otherwise agreed, a picture credit identifying
#   the photographer must accompany each reproduction of an image, but no
#   picture credits may be used for images altered or manipulated other than by
#   cropping, enlarging, "dodging and burning" or changing overall color balance.
#
#                   EASTMAN KODAK COMPANY Rochester, NY 14650

set -euo pipefail

echo "Running regression test..."

if ! ./pcdinfo tests/info.pcd | diff -u - tests/expected_output; then
    echo "Failed"
    exit 1
fi

echo "OK"
