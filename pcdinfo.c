/*
 * pcdinfo print metadata from Kodak Photo CD info files "info.pcd"
 * Copyright (C) 2023  Tobias Rautenkranz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "pcdinfo.h"

/* from binary coded decimal */
static uint8_t
from_bcd(uint8_t bcd)
{
  const uint8_t n1 = (bcd & 0xF0) >> 4;
  const uint8_t n2 = bcd & 0x0F;
  assert(n1 < 10);
  assert(n2 < 10);

  return n1*10 + n2;
}

int
main(int argc, char *argv[])
{
  if (argc != 2) {
    printf("Usage: pcdinfo INFO.PCD\n"
           "Prints some metadate for a Kodak Photo CD info file.\n");
    return EXIT_FAILURE;
  }

  FILE* fp = fopen(argv[1], "r");
  if (!fp) {
    perror("pcdinfo: open failed");
    return EXIT_FAILURE;
  }

  struct pcd_info_file pcdinfo;

  size_t ret = fread(&pcdinfo, 1, sizeof(pcdinfo), fp);
  if (ret != sizeof(pcdinfo)) {
    fprintf(stderr, "pcdinfo: Not a info.pcd file: too short\n");
    fclose(fp);
    return EXIT_FAILURE;
  }

  if (fclose(fp)) {
    perror("fclose failed");
  }

  const char signature[8] = "PHOTO_CD";
  if (memcmp(pcdinfo.signature, signature, sizeof(signature))) {
    fprintf(stderr, "pcdinfo: Not a info.pcd file: invalid signature\n");
    return EXIT_FAILURE;
  }

  printf("revision                   %d.%d\n", pcdinfo.majorRevision,
         pcdinfo.minorRevision);
  printf("serial number              %.*s\n",
         (int) sizeof(pcdinfo.serialNumber), pcdinfo.serialNumber);

  time_t t = be32toh(pcdinfo.creationTime);
  printf("creation time              %s", asctime(localtime(&t)));

  t = be32toh(pcdinfo.modificationTime);
  printf("modification time          %s", asctime(localtime(&t)));

  printf("number of images           %d\n", be16toh(pcdinfo.nImages));
  printf("interleave ratio           %d\n", pcdinfo.interleaveRatio);
  printf("resolution                 0x%.2x\n", pcdinfo.resolution);

  // The position is specified in mm:ss:ff (minute-second-frame) format. There are 75 such frames per second of audio.
  printf("lead out start time        %.2d:%.2d:%.2d\n",
         from_bcd(pcdinfo.leadOutStartTime[0]),
         from_bcd(pcdinfo.leadOutStartTime[1]),
         from_bcd(pcdinfo.leadOutStartTime[2]));

  printf("number of sessions         %d\n", pcdinfo.nSessions);

  return EXIT_SUCCESS;
}
