all: pcdinfo

pcdinfo: pcdinfo.c pcdinfo.h
	$(CC) -o $@ -lc $<


.PHONY: check
check: pcdinfo
	./tests/run_regression.sh

.PHONY: clean
clean:
	rm -f -- pcdinfo
