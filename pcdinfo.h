#ifndef PCDINFO_H_
#define PCDINFO_H_

#include <inttypes.h>

/*
 * Kodak Photo CD info.pcd file layout.
 * Big endian encoding for uint16_t and uint32_t
 *
 * adapted from:
 * Ultimedia Services Version 2 for AIX: Programmer's Guide and Reference
 * UMSPCDImageReader Object
 * https://sites.ualberta.ca/dept/chemeng/AIX-43/share/man/info/C/a_doc_lib/ultimdia/ultiprgd/UMSPCDImageReader.htm
 */
struct
__attribute__((__packed__)) // needs to be packed to prevent gaps due to uint32_t alignment
pcd_info_file
{
   char      signature[8]; // Must be PHOTO_CD
   uint8_t   majorRevision;
   uint8_t   minorRevision;
   char      serialNumber[12];
   uint32_t  creationTime; // unix time
   uint32_t  modificationTime; // unix time
   uint16_t  nImages;
   uint8_t   interleaveRatio; // always 1?
   uint8_t   resolution; // unknown
   uint8_t   leadOutStartTime[3]; // binary coded decimal (minute, seconds, frame)
   uint8_t   nSessions;
};


#endif // PCDINFO_H_
